<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


////////////////////////////////////////////////////////
//  ***********************************************  //
//              Resourceful Endpoints                //
//  ***********************************************  //
///////////////////////////////////////////////////////

Route::resource('user', 'UserController');
Route::resource('courses', 'CoursesController');
Route::resource('level', 'LevelController');
Route::resource('personalities', 'PersonalityController');
Route::resource('questions', 'QuestionController');
Route::resource('semester', 'SemesterController');
Route::resource('academic_details', 'AcademicDetailsController');
Route::resource('user_personality', 'UserPersonalityController');
Route::resource('user_courses', 'UserCoursesController');

////////////////////////////////////////////////////////
//  ***********************************************  //
//              Un-resourceful Endpoints              //
//  ***********************************************  //
///////////////////////////////////////////////////////
Route::post('access_level', 'UserController@accessLevel');
Route::post('determine_personality', 'PersonalityController@determinePersonality');
