<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicDetails extends Model
{
    protected $fillable = [
        'user_id', 'level', 'semester'
    ];


    // Relational function to indicate user
    public function userAcademics() {
        return $this->belongsTo(User::class);
    }
}
