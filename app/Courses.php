<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $fillable = [
        'course_name', 'course_code', 'credit_hrs', 'level_id', 'semester_id'
    ];

    public function level() {
        return $this->hasMany(Level::class, 'level_id');
    }

    public function semester() {
        return $this->hasMany(Semester::class, 'semester_id');
    }
}
