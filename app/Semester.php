<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    protected $fillable = [
        'name', 'startDate', 'endDate'
    ];

    public function course() {
        return $this->belongsTo(Courses::class, 'course_id');
    }

    public function level() {
        return $this->belongsTo(Leve::class, 'level_id');
    }
}
