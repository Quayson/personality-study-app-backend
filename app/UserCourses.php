<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCourses extends Model
{
    protected $fillable = [
        'user_id', 'course_name', 'course_code', 'credit_hrs', 'level_id', 'semester_id'
    ];
}
