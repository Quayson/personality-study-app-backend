<?php

namespace App\Http\Controllers;

use App\AcademicDetails;
use Illuminate\Http\Request;
use App\Courses;
use App\UserCourses;
use App\Semester;

class AcademicDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $academicDetails = AcademicDetails::get();
        return response(compact('academicDetails'), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        AcademicDetails::create($request->all());

        $params = json_decode($request->getContent(), true);

        $courses = Courses::where('level_id', $params['level'])
            ->where('semester_id', $params['semester'])
            ->get();

        // $semData = Semester::where('id', $params['semester'])->first();

        $user_courses = [];

        if (!empty($courses->toArray())) {
            $user_courses = $courses;
        }

        foreach ($user_courses as $key => $user_course) {
            $user_course->user_id = $params['user_id'];
            UserCourses::create($user_course->toArray());
        }

        $message = 'Academic data added successfully';
        return response(compact('message'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AcademicDetails  $academicDetails
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data = AcademicDetails::where('user_id', $id)->get();

        // $data = [];

        // return sizeOf($data);

        if (sizeOf($data) === 0) {
            return response(compact('data'), 200);
        }

        $semID = $data[0]->semester;

        $semData = Semester::where('id', $semID)->get()->first();
        return response()->json([
            'data' => $data,
            'semester' => $semData
        ], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AcademicDetails  $academicDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(AcademicDetails $academicDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AcademicDetails  $academicDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        AcademicDetails::updateOrCreate(['user_id'=>$id],$request->all());
        $message ="Academic information updated successfully";
        return response(compact('message'),200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AcademicDetails  $academicDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(AcademicDetails $academicDetails)
    {
        //
    }
}
