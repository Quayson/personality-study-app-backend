<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Http\Client\Exception;
use App\AcademicDetails;
use App\Courses;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()->get();
        return response(compact('users'), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::create($request->all());
        $message = "New user created successfully";
        return response(compact('message'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = User::where('id',$request->$id)->get();
        return response(compact('user'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $user = User::updateOrCreate(['user_id'=>$id],$request->all());
        $message ="User information updated successfully";
        return response(compact('message'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        $message = "User Deleted successfully";
        return response(compact('message'), 200);
    }


    // Function for determining user access level
    public function accessLevel(Request $request)
    {
        $user = User::where('email', $request->username)->first();
        return response(compact('user'), 200);
    }

     // setting user to get and retrieve from user details table
     public function userDetails(){
        return $this->hasOne(UserDetails::class);
    }

    // setting user to get and retrieve from work details table
    public function academicDetails(){
        return $this->hasOne(UserAcademics::class);
    }

    // user courses
    public function course(Request $request) {
        $courses = Courses::where(['level']);
    }
}
