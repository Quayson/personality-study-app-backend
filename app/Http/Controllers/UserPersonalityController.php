<?php

namespace App\Http\Controllers;

use App\UserPersonality;
use Illuminate\Http\Request;
use App\Personality;

class UserPersonalityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personality = UserPersonality::get();
        return response(compact('personality'), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        UserPersonality::create($request->all());
        $message = 'User personality determined';
        return response(compact('message'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserPersonality  $userPersonality
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $personality = UserPersonality::where('user_id', $id)->get()->first();
        return response(compact('personality'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserPersonality  $userPersonality
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPersonality $userPersonality)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserPersonality  $userPersonality
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        UserPersonality::updateOrCreate(['user_id' => $id], $request->all());
        $message = "User personality data updated successfully";
        return response(compact('message'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserPersonality  $userPersonality
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPersonality $userPersonality)
    {
        //
    }
}
