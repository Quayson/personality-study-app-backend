<?php

namespace App\Http\Controllers;

use App\Personality;
use App\UserPersonality;
use Illuminate\Http\Request;
use function GuzzleHttp\json_decode;
use Illuminate\Support\Arr;

class PersonalityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personalities = Personality::get();
        return response(compact('personalities'), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Personality::create($request->all());
        $message = 'New personality added';
        return response(compact('message'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Personality  $personality
     * @return \Illuminate\Http\Response
     */
    public function show(Personality $personality, Request $request, $id)
    {
        $data = Personality::where('id', $request->$id)->get();
        return response(compact('data'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Personality  $personality
     * @return \Illuminate\Http\Response
     */
    public function edit(Personality $personality)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Personality  $personality
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Personality::updateOrCreate(['personality_id' => $id], $request->all());
        $message = "Personalty trait updated successfully";
        return response(compact('message'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Personality  $personality
     * @return \Illuminate\Http\Response
     */
    public function destroy(Personality $personality)
    {
        //
    }

    public function determinePersonality(Request $request)
    {

        $answersID = [];
        $count = 0;

        $params = json_decode($request->getContent(), true);

        // return $params;

        $data1 = Personality::where('question_1', $params['question_1'])->get();
        $data2 = Personality::where('question_2', $params['question_2'])->get();
        $data3 = Personality::where('question_3', $params['question_3'])->get();
        $data4 = Personality::where('question_4', $params['question_4'])->get();
        $data5 = Personality::where('question_5', $params['question_5'])->get();
        $data6 = Personality::where('question_6', $params['question_6'])->get();
        $data7 = Personality::where('question_7', $params['question_7'])->get();
        $data8 = Personality::where('question_8', $params['question_8'])->get();
        $data9 = Personality::where('question_9', $params['question_9'])->get();
        $data10 = Personality::where('question_10', $params['question_10'])->get();

        // return $data8;

        if (!empty($data1) || $data1 !== null) {
            $did =  $data1[0]['id'];
            $answersID[$count++] = $did;
            //    return $answersID;
        }
        if (!empty($data2) || $data2 !== null) {
            $did =  $data2[0]['id'];
            $answersID[$count++] = $did;
            //    return $answersID;
        }
        if (!empty($data3) || $data3 !== null) {
            $did =  $data3[0]['id'];
            $answersID[$count++] = $did;
            //    return $answersID;
        }
        if (!empty($data4) || $data4 !== null) {
            $did =  $data4[0]['id'];
            $answersID[$count++] = $did;
            //    return $answersID;
        }
        if (!empty($data5) || $data5 !== null) {
            $did =  $data5[0]['id'];
            $answersID[$count++] = $did;
            //    return $answersID;
        }
        if (!empty($data6) || $data6 !== null) {
            $did =  $data6[0]['id'];
            $answersID[$count++] = $did;
            //    return $answersID;
        }
        if (!empty($data7) || $data7 !== null) {
            $did =  $data7[0]['id'];
            $answersID[$count++] = $did;
            //    return $answersID;
        }
        if (!empty($data8) && $data8 !== null) {
            $did =  $data8[0]['id'];
            $answersID[$count++] = $did;
            //    return $answersID;
        }
        if (!empty($data9) || $data9 !== null) {
            $did =  $data9[0]['id'];
            $answersID[$count++] = $did;
            // return $answersID;
        }
        if (!empty($data10) || $data10 !== null) {
            $did =  $data10[0]['id'];
            $answersID[$count++] = $did;
            // return $answersID;
        }

        // Counting the number of times an id reoccurred in the $answers array
        $countOccurences = array_count_values($answersID);

        // Finding id with max values in it
        $personalityID = array_search(max($countOccurences), $countOccurences);

        // Getting personality with new ID gotten

        $data = Personality::where('id', $personalityID)->get();


        $user_personalities = [];

        if (!empty($data->toArray())) {
            $user_personalities = $data;
        }

        foreach ($user_personalities as $key => $user_personality) {
            $user_personality->user_id = $params['user_id'];
            UserPersonality::create($user_personality->toArray());
        }

        $message = 'Congratulations, test successful.';
        return response(compact('message'), 200);


    }
}
