<?php

namespace App\Http\Controllers;

use App\UserCourses;
use Illuminate\Http\Request;

class UserCoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = UserCourses::get();
        return response(compact('data'), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        UserCourses::create($request->all());
        $message = 'Courses created successfully';
        return response(compact('message'), 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserCourses  $userCourses
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data = UserCourses::where('user_id', $id)->get();
        return response(compact('data'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserCourses  $userCourses
     * @return \Illuminate\Http\Response
     */
    public function edit(UserCourses $userCourses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserCourses  $userCourses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        UserCourses::updateOrCreate(['user_id'=>$id],$request->all());
        $message ="User Courses data updated successfully";
        return response(compact('message'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserCourses  $userCourses
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCourses $userCourses)
    {
        //
    }
}
