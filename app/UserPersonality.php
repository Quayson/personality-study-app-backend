<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPersonality extends Model
{
    protected $fillable = [
        'user_id', 'name', 'study_form'
    ];
}
