<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $fillables = [
        'name'
    ];


    public function course() {
        return $this->belongsTo(Course::class, 'course_id');
    }

    public function semester() {
        return $this->hasMany(Semester::class, 'semester_id');
    }

}
